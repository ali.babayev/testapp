import 'package:flutter/material.dart';

abstract class AppTheme {
  static final ThemeData themeData = ThemeData(
    primarySwatch: AppColors.mainColor,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    scaffoldBackgroundColor: Colors.white,
  );
}

abstract class AppColors {
  static const mainColor = Colors.blue;
  static const secondaryColor = Color(0xFF7E57C2); // deepPurple[400]
  static const alternateColor = Colors.white;
}

abstract class AppTextStyles {
  static const TextStyle menuTextStyle = TextStyle(
    color: AppColors.alternateColor,
    fontSize: 18,
  );

  static final TextStyle titleBold = TextStyle(
    fontSize: 50,
    fontWeight: FontWeight.bold,
    color: AppColors.mainColor[900],
  );
}
