class Dimensions {
  static const double buttonSize = 56.0;
  static const double menuItemHeight = 50.0;
  static const double menuItemWidth = 150.0;
  static const double navigationBarHeight = 100.0;
}