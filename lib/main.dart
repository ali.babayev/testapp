import 'package:flutter/material.dart';
import 'package:testapp/style/app_style.dart';
import 'package:testapp/presentation/homepage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test App',
      theme: AppTheme.themeData,
      home: const HomePage(),
    );
  }
}
