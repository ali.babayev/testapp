import 'dart:math';
import 'package:flutter/material.dart';
import 'package:testapp/constants/dimensions.dart';
import 'package:testapp/presentation/widgets/animated_screen_painter.dart';
import 'package:testapp/presentation/widgets/custom_navigation_bar.dart';
import 'package:testapp/presentation/widgets/menu_item.dart';
import 'package:testapp/style/app_style.dart';
import 'package:testapp/enums/button_enums.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  final List<String> _menuItems = [
    'Reminder',
    'Camera',
    'Attachment',
    'Note',
  ];

  // final double Dimensions.buttonSize = 56;
  // final double Dimensions.menuItemHeight = 50;
  // final double Dimensions.menuItemWidth = 150;
  // final double Dimensions.navigationBarHeight = 100;

  late double maxButtonY;
  late AnimationController _controller;
  Animation<double>? _animation;

  double buttonY = 0;
  int tabIndex = 0;
  CentralButtonType buttonType = CentralButtonType.plus;

  @override
  void initState() {
    super.initState();
    maxButtonY = _menuItems.length * Dimensions.menuItemHeight +
        Dimensions.menuItemHeight;
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
  }

  void _changeTab(int index) {
    debugPrint("Tab changed index: $index");
    setState(() {
      tabIndex = index;
    });
  }

  void _openMenu() {
    _animation = Tween<double>(begin: 0, end: maxButtonY).animate(_controller)
      ..addListener(() {
        setState(() {
          buttonY = _animation!.value;
        });
      });
    _controller.forward().then((value) {
      setState(() {
        buttonType = CentralButtonType.cancel;
      });
    });
  }

  void _closeMenu() {
    if (_controller.isAnimating) {
      // If already animating, return early.
      return;
    }

    if (!mounted) {
      // If the widget is not currently in the tree, return early.
      return;
    }

    _controller.duration = const Duration(milliseconds: 250);
    _controller.reverse().then((value) {
      if (!mounted) {
        // If the widget is not currently in the tree, return early.
        return;
      }
      _controller.duration =
          const Duration(milliseconds: 500); // Reset to the initial speed
      setState(() {
        buttonType = CentralButtonType.plus;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    const buttonBottomPadding =
        (Dimensions.navigationBarHeight - Dimensions.buttonSize) / 2;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: Text(
              'Events',
              style: AppTextStyles.titleBold,
            ),
          ),

          /// Custom Navigation Bar
          Positioned(
            bottom: 0,
            child: CustomNavigationBar(
              onTabChange: _changeTab,
              tabIndex: tabIndex,
              centralButtonSize: Dimensions.buttonSize,
              navBarHeight: Dimensions.navigationBarHeight,
            ),
          ),

          /// Animated Water Drop style screen filler Widget
          _controller.value > 0
              ? AnimatedScreenPainterWidget(
                  controller: _controller,
                  buttonType: buttonType,
                  buttonY: buttonY,
                  maxButtonY: maxButtonY,
                  buttonSize: Dimensions.buttonSize,
                  buttonBottomPadding: buttonBottomPadding,
                )
              : const SizedBox(),

          /// Menu Items
          ...List.generate(
            _menuItems.length,
            (index) {
              return Positioned(
                bottom: maxButtonY - Dimensions.menuItemHeight * (index + 1),
                right: screenWidth / 2 - Dimensions.menuItemWidth / 2,
                left: screenWidth / 2 - Dimensions.menuItemWidth / 2,
                child: _controller.value > 0.8
                    ? MenuItem(
                        text: _menuItems[index],
                        onTap: () => debugPrint("Pressed ${_menuItems[index]}"),
                        menuItemWidth: Dimensions.menuItemWidth,
                        menuItemHeight: Dimensions.menuItemHeight,
                      )
                    : const SizedBox(),
              );
            },
          ),

          /// Central button to open and close menu
          Positioned(
            bottom: buttonY + buttonBottomPadding,
            right: (screenWidth - Dimensions.buttonSize) / 2,
            child: GestureDetector(
              onVerticalDragUpdate: (details) {
                /// Remove IF statament if you want to drag button towards bottom as well
                if (buttonType == CentralButtonType.plus) {
                  setState(() {
                    buttonY -= details.delta.dy;
                    _controller.value = buttonY / maxButtonY;
                    if (_controller.value > 1.0) {
                      _controller.value = 1.0;
                    }
                    if (buttonY > maxButtonY) {
                      buttonY = maxButtonY;
                    } else if (buttonY < 0) {
                      buttonY = 0;
                    }
                  });
                }
              },
              onVerticalDragEnd: (details) {
                if (buttonY > 0) {
                  if (buttonType == CentralButtonType.plus) {
                    _openMenu();
                  }

                  /// Uncomment if you want to drag button towards bottom as well
                  // else {
                  //   _closeMenu();
                  // }
                }
              },
              onTap: buttonType == CentralButtonType.plus
                  ? () => _openMenu()
                  : () => _closeMenu(),
              child: AnimatedBuilder(
                animation: _controller,
                builder: (context, child) {
                  return Transform.rotate(
                    angle: _controller.value * 0.25 * pi,
                    child: Container(
                      height: Dimensions.buttonSize,
                      width: Dimensions.buttonSize,
                      decoration: BoxDecoration(
                        color: _controller.value == 1
                            ? AppColors.alternateColor
                            : AppColors.mainColor,
                        borderRadius:
                            BorderRadius.circular(Dimensions.buttonSize * 2),
                      ),
                      child: Icon(
                        Icons.add,
                        size: Dimensions.buttonSize / 2,
                        color: _controller.value == 1
                            ? AppColors.mainColor
                            : AppColors.alternateColor,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
