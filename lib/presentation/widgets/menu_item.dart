import 'package:flutter/material.dart';
import 'package:testapp/style/app_style.dart';

class MenuItem extends StatelessWidget {
  final String text;
  final VoidCallback onTap;

  final double menuItemHeight;
  final double menuItemWidth;

  const MenuItem({
    super.key,
    required this.text,
    required this.onTap,
    required this.menuItemHeight,
    required this.menuItemWidth,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: 1.0,
      duration: const Duration(milliseconds: 500),
      child: Center(
        child: SizedBox(
          height:menuItemHeight,
          width: menuItemWidth,
          child: TextButton(
            onPressed: onTap,
            child: Text(
              text,
              style: AppTextStyles.menuTextStyle,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
