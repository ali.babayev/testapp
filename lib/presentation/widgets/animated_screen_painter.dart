import 'dart:math';

import 'package:flutter/material.dart';
import 'package:testapp/enums/button_enums.dart';
import 'package:testapp/style/app_style.dart';

class AnimatedScreenPainterWidget extends StatelessWidget {
  const AnimatedScreenPainterWidget({
    super.key,
    required AnimationController controller,
    required this.buttonType,
    required this.buttonY,
    required this.maxButtonY,
    required this.buttonSize,
    required this.buttonBottomPadding,
  }) : _controller = controller;

  final AnimationController _controller;
  final CentralButtonType buttonType;
  final double buttonY;
  final double maxButtonY;
  final double buttonSize;
  final double buttonBottomPadding;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Container(
          color: AppColors.mainColor.withOpacity(
            _controller.value < 0.5 ? _controller.value * 2 : 1.0,
          ),
          child: buttonType == CentralButtonType.cancel
              ? Container(
                  color: AppColors.mainColor.withOpacity(_controller.value),
                )
              : CustomPaint(
                  painter: MyCustomPainter(
                    color: AppColors.alternateColor.withOpacity(
                      _controller.value < 0.5 ? _controller.value * 2 : 1.0,
                    ),
                    yPos: buttonY,
                    maxY: maxButtonY,
                    controllerValue: _controller.value,
                    buttonSize: buttonSize,
                    buttonBottomPadding: buttonBottomPadding,
                  ),
                  child: Container(),
                ),
        );
      },
    );
  }
}

class MyCustomPainter extends CustomPainter {
  final Color color;
  final double yPos; // y position of the draggable button
  final double maxY; // y position of the bottom of the screen
  final double controllerValue; // The x offset of the curve
  final double buttonSize; // A value to scale the curve
  final double buttonBottomPadding; // A value to scale the curve

  MyCustomPainter({
    required this.color,
    required this.yPos,
    required this.maxY,
    required this.controllerValue,
    required this.buttonSize,
    required this.buttonBottomPadding,
  });

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    double offset = 10 * (0.4 - controllerValue);

    var leftControlPoint = Offset(
        (size.width - buttonSize * 1.2) / 2 + controllerValue * 10,
        size.height);
    var rightControlPoint = Offset(
        (size.width + buttonSize * 1.2) / 2 - controllerValue * 10,
        size.height);

    Offset leftEndPoint;
    Offset arcCenter;
    double arcHeight;
    double arcWidth;
    if (controllerValue < 0.6) {
      leftEndPoint = Offset(
        size.width / 2 - buttonSize / 2 - offset,
        size.height -
            buttonBottomPadding -
            buttonSize / 2 -
            maxY * controllerValue,
      );
      arcCenter = Offset(
        size.width / 2,
        size.height -
            buttonBottomPadding -
            buttonSize / 2 -
            maxY * controllerValue,
      );
      arcHeight = buttonSize + offset;
      arcWidth = buttonSize + offset * 2;
    } else {
      leftEndPoint = Offset(
        size.width / 2 - buttonSize / 4,
        size.height -
            (buttonBottomPadding + buttonSize / 2 + maxY) *
                (1 - controllerValue),
      );
      arcCenter = Offset(
        size.width / 2,
        size.height -
            (buttonBottomPadding + buttonSize / 2 + maxY) *
                (1 - controllerValue),
      );
      arcHeight = controllerValue >= 0.98
          ? buttonSize / 2 * (1 - controllerValue) * 40
          : buttonSize / 2;
      arcWidth = (buttonSize / 2);
    }
    var rightEndPoint = Offset(size.width, size.height);

    var path = Path();
    path.moveTo(0, size.height);
    path.quadraticBezierTo(
      leftControlPoint.dx,
      leftControlPoint.dy,
      leftEndPoint.dx,
      leftEndPoint.dy,
    );

    path.arcTo(
      Rect.fromCenter(
        center: arcCenter,
        height: arcHeight,
        width: arcWidth,
      ),
      pi,
      pi,
      false,
    );

    path.quadraticBezierTo(
      rightControlPoint.dx,
      rightControlPoint.dy,
      rightEndPoint.dx,
      rightEndPoint.dy,
    );

    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
