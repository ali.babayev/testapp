import 'package:flutter/material.dart';
import 'package:testapp/style/app_style.dart';

class CustomNavigationBar extends StatelessWidget {
  final Function(int) onTabChange;
  final int tabIndex;
  final double centralButtonSize;
  final double navBarHeight;

  const CustomNavigationBar({
    super.key,
    required this.onTabChange,
    required this.tabIndex,
    required this.navBarHeight,
    required this.centralButtonSize,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: navBarHeight,
      width: MediaQuery.of(context).size.width,
      color: AppColors.alternateColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          IconButton(
            icon: const Icon(Icons.calendar_today_rounded,
                color: AppColors.secondaryColor),
            onPressed: () => onTabChange(0),
          ),
          IconButton(
            icon: const Icon(Icons.search, color: AppColors.secondaryColor),
            onPressed: () => onTabChange(1),
          ),
          SizedBox(width: centralButtonSize),
          IconButton(
            icon: const Icon(Icons.flash_on_outlined,
                color: AppColors.secondaryColor),
            onPressed: () => onTabChange(2),
          ),
          IconButton(
            icon: const Icon(Icons.person, color: AppColors.secondaryColor),
            onPressed: () => onTabChange(3),
          ),
        ],
      ),
    );
  }
}
